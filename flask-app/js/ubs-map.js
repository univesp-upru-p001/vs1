async function initMap() {
    const center = { lat: -23.5406, lng: -46.6321 };
    const map = new google.maps.Map(document.getElementById('map'), {
      zoom: 10,
      center: center,
    });
  
    try
    {
      response = await fetch('/ubs/json')
      data = await response.json()
      await console.log(data)

      data = data.ubs.filter( item => item.latitude != null && item.longitude != null ) 
      await console.log(data)

      const markers = data.map((item, index) => {
          const marker = new google.maps.Marker({
            position: {
              lat : item.latitude,
              lng : item.longitude
            },
            label: item.district
        })
    
        return marker;
      })

      new markerClusterer.MarkerClusterer({ markers, map })
    }
    catch (exception)
    {
      console.log(exception)
    }
  }
  