import os
import flask
import datetime
import dateutil
import traceback
import pymysql
import pandas


app = flask.Flask(__name__)

app.run(host='0.0.0.0', port=5000, debug=True)


def db_connection():
    db = pymysql.connect(  host=os.environ.get('APPSETTING_DATABASE_HOST'),                     # your host, usually localhost
                           user=os.environ.get('APPSETTING_DATABASE_USER'),                     # your username
                         passwd=os.environ.get('APPSETTING_DATABASE_PASSWD'),                   # your password
                             db='projeto_integrador_bd',                                        # name of the data base
                     cursorclass=pymysql.cursors.DictCursor)                                                   

    return db


@app.route('/js/<path:path>')
def send_js(path):
    return flask.send_from_directory('js', path)


@app.route('/css/<path:path>')
def send_css(path):
    return flask.send_from_directory('css', path)


@app.route('/img/<path:path>')
def send_img(path):
    return flask.send_from_directory('img', path)


@app.route('/release')
def release():
    return flask.render_template('release.html')


@app.route('/database')
def database():
    result = 'ok'
    try:
        db = db_connection()

    except BaseException as exception:
        result = f'<<{os.environ.get("APPSETTING_DATABASE_HOST")}>>' + '\n' + str(exception)

    return flask.render_template('database.html', information=result)


@app.route('/')
def home():
    return "Teste"
    #return flask.render_template('home.tmpl')


@app.route('/vacinas')
def vacines():
    #birthdate = dateutil.parser.parse(flask.request.args.get('birthdate'))
    birthdate = datetime.date.fromisoformat(flask.request.args.get('birthdate'))

    try:
        with db_connection() as connection:
            with connection.cursor() as cursor:
                cursor.execute('SELECT * FROM tb_calendario_vacina')
                rows = cursor.fetchall()
                
                list = []
                for row in rows:
                    target = birthdate + dateutil.relativedelta.relativedelta(months = row['meses'])
                    list.append([row['descricao_vacina'], row['meses'], target])

                list.sort(key = lambda element : element[2])
                
                view = []
                for index in range(len(list) - 5):
                    view.append([ index+1, list[index][0], list[index][2].strftime('%d/%m/%Y') ])
                

        return flask.render_template('vacines.tmpl', seq = view)

    except BaseException as exception:
        result = f'<<{os.environ.get("APPSETTING_DATABASE_HOST")}>>' + '\n' + str(exception)
        raise exception


@app.route('/ubs1')
def ubs1():
    try:
        with db_connection() as connection:
            with connection.cursor() as cursor:
                # cursor.execute('select * from tb_ubs_dados_brasil WHERE uf = 35 order by bairro')
                cursor.execute('select * from view_pesquisa_ubs_2')
                rows = cursor.fetchall()
                
                list = []
                for row in rows:
                    list.append([row['nome'], row['bairro'], row['endereço']])

                #list.sort(key = lambda element : element[2])
                
                view = []
                for index in range(len(list) - 1):
                    view.append([ index+1, list[index][0], list[index][1], list[index][2] ])
                

        return flask.render_template('ubs1.html', seq = view)

    except BaseException as exception:
        result = f'<<{os.environ.get("APPSETTING_DATABASE_HOST")}>>' + '\n' + str(exception)
        raise exception    


@app.route('/ubs')
def ubs2():
    return flask.render_template('ubs-map.html')


@app.route('/ubs/json')
def ubs_json():
    try:
        with db_connection() as connection:
            with connection.cursor() as cursor:
                # cursor.execute('select * from tb_ubs_dados_brasil WHERE uf = 35 order by bairro')
                cursor.execute('select * from view_pesquisa_ubs_2')
                rows = cursor.fetchall()
                
                list = []
                for row in rows:
                    list.append({
                        'name'       : row['nome'      ],
                        'district'   : row['bairro'    ],
                        'address'    : row['endereço'  ],
                        'latitude'   : float(row['latitude' ]) if not row['latitude' ] == '' else None,
                        'longitude'  : float(row['longitude']) if not row['longitude'] == '' else None
                    })
           
        return flask.jsonify(ubs=list)

    except BaseException as exception:
        result = f'<<{os.environ.get("APPSETTING_DATABASE_HOST")}>>' + '\n' + str(exception)
        raise exception 


@app.route('/home')
def oldhome():
    askdate = """
        <form method="GET" action='/idade'  onsubmit="return validate(this)" >
            Digite a data de nascimento:<br />
            <input type="date" name="ano_nasc"  value="2013-11-12" />
            <br><br>
            <input type="submit" value="Calcular" />
        </form>
        <script src="js/start.js"></script>"""

    return flask.render_template('-askdate.html', title = 'Data de nascimento', body = askdate)


@app.route('/idade')
def age():
    db = None
    try:
        nascimento = flask.request.args.get('ano_nasc')      
        nova_data = dateutil.parser.parse(nascimento)                                               # transfoma a dataa para o formato intenacional
        
        db = db_connection()
        dados_sql=pandas.read_sql ("SELECT * FROM tb_calendario_vacina;", con = db )   # cria objeto com o select do banco de dados
        dados_sql.index_col = False
        
        #raise BaseException('Isto é uma exceção forçada para teste de captura do traceback na pagina HTML')
        
        conta_mes = 0                                                                               # Contador para o for 
        listadata = []                                                                              # lista vazia que vai receber os valores de (data + meses)
        diasfalta = []
        for (row,rs) in dados_sql.iterrows():                                                       # percorre todas as linhas da tabela
            quantidade_mes =int(dados_sql['meses'].values[conta_mes])                               # recebe a quntidade de mes e coloca na quantidade_mes 
            #adiciona a quaantidade de mêses na data
            
            data_prevista = pandas.to_datetime(nova_data) + pandas.DateOffset(days=quantidade_mes)  # data_prevista = nova_data + relativedelta(months = quantidade_mes)
            #dias  =(data_prevista  - date.today())
            
            conta_mes += 1                                                                          # incrementa o contador
            listadata += [data_prevista]
            #diasfalta += [dias]
        
        dados_sql['dataprevista']= listadata                                                        # adiciona a lista ao dataframe
        dados_sql['status']=''
        dados_sql.to_string(index=False)

        dados_sql['dataprevista'] = pandas.to_datetime(dados_sql['dataprevista'])                   # transforma data para o formato brasileiro 
        calendario_vacina = dados_sql.sort_values(by=['dataprevista'], ascending=True)              # ordena o dataset com o caampo "dataprevista" crescente
        
        #calendario_vacina_filtro = calendario_vacina[['descricao_vacina','dataprevista','status']]
        calendario_vacina_filtro = calendario_vacina[['descricao_vacina','dataprevista']] 
        calendario_vacina_filtro['dataprevista'] = calendario_vacina_filtro['dataprevista'].dt.strftime('%d/%m/%Y')   
        calendario_vacina_filtro.columns=['Descrição da Vacina', 'Data Prevista'] 
      
        body = calendario_vacina_filtro.to_html(header="true", table_id="table")

        return flask.render_template('-skeleton.html', title = "Datas previstas para ser Vacinado 1" , body = body)  

    except BaseException as exception:
        traceback.print_exc()
        return flask.render_template('-skeleton.html', title = "Traceback" , body = '<pre>{0}</pre>'.format(traceback.format_exc()))

    finally:
        db.close()
        
