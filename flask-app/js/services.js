
(() => {
    const FONT_SIZE_LOW  = 1
    const FONT_SIZE_HIGH = 5


    function switchContrast() {
        try {
            for (const element of document.querySelectorAll('body, p, .card, tr, .table-striped > tbody > tr:nth-of-type(odd)')) {
                element.classList.toggle('dark');
            }
    
             const navbar = document.querySelector('.navbar')
             navbar.classList.toggle('bg-light')
             navbar.classList.toggle('dark')

             const icons = document.querySelectorAll('.tools img')
             for (const icon of icons) {
                icon.classList.toggle('icon-inverted')
                icon.classList.toggle('icon-not-inverted')
             }

             if (document.querySelector('#contrast img').classList.contains('icon-inverted')) {
                localStorage.setItem('highContrast', 'on'  )
             } else {
                localStorage.setItem('highContrast', 'off' )
             }
        }
        catch (error) {
            console.log(error)
        }
    }

    function setFontSizePattern(size) {
        localStorage.setItem('font-size', size)

        for (const element of document.querySelectorAll('p, .card, button, tr')) {
            const list = element.classList
            for(const item of list) {
                if (/^font-size-class-a/.test(item)) {
                    list.remove(item)
                }
            }
            list.add('font-size-class-a-' + size)
        }
    }

    function fontDecrease() {
        try {
            let size

            size = parseInt(localStorage.getItem('font-size'))
            if (size == FONT_SIZE_LOW)
                return
    
            size--
            setFontSizePattern(size)
        } catch (error) {
            console.log(error)
        }  
    }

    function fontIncrease() {
        let size

        size = parseInt(localStorage.getItem('font-size'))
        if (size == FONT_SIZE_HIGH)
            return

        size++
        setFontSizePattern(size)
    }

    console.log(document.readyState)
    document.addEventListener('DOMContentLoaded', () => {
        document.getElementById('contrast').addEventListener('click', switchContrast)
        document.getElementById('font-decrease').addEventListener('click', fontDecrease)
        document.getElementById('font-increase').addEventListener('click', fontIncrease)

        if ( !['on', 'off'].includes(localStorage.getItem('highContrast')) ) {
            localStorage.setItem('highContrast', 'off')
        }

        const icons = document.querySelector('#contrast img')
        switch (localStorage.getItem('highContrast'))
        {
            case 'on' : 
                switchContrast()
                break;

            case 'off':
                break;
        }

        if ( ![ 1, 2, 3, 4, 5 ].includes(parseInt(localStorage.getItem('font-size'))) ) {
            localStorage.setItem('font-size', 2)
        }

        setFontSizePattern(localStorage.getItem('font-size'))
    })

    if (document.readyState != 'loading' ) {
        window.dispatchEvent(new Event("DOMContentLoaded"))
    }
        
})()