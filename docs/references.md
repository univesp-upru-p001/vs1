## References

font-size  
https://developer.mozilla.org/en-US/docs/Web/CSS/font-size

Icons DB  
https://www.iconsdb.com

Use different accounts on a single GitLab instance
https://docs.gitlab.com/ee/user/ssh.html#use-different-accounts-on-a-single-gitlab-instance

Favicon Converter
https://favicon.io/favicon-converter

