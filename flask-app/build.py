from datetime           import datetime
from backports.zoneinfo import ZoneInfo

now = datetime.now(ZoneInfo('America/Sao_Paulo'))

with open('templates/release.html', mode='w') as output:
    row = 1
    try:
        for line in open('release.template', mode='r').readlines():
            output.write(eval(f'f{line!r}'))
            row += 1
    except Exception as e:
        print(f'row is', row)
        raise e
